import os.path
from unittest.mock import Mock


class FakeFile:
    def __init__(self, content):
        self.content = content

    def __enter__(self, ):
        file = Mock()
        file.read.return_value = self.content

        return file

    def __exit__(self, type, value, traceback):
        pass


class StubFS:
    def __init__(self, path):
        """Stub a FS system
        """
        self._path = path
        self._files = {}
        self.open_calls = 0

    def getsyspath(self, filename):
        return os.path.join(self._path, filename)

    def create_file(self, filename, content):
        self._files[filename] = FakeFile(content)

    def open(self, filename, *options):
        self.open_calls += 1
        return self._files[filename]

    def exists(self, filename):
        return filename in self._files
