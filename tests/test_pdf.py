from unittest.mock import patch
from conteng.pdf import PdfContentCreator
from tests.builders import StubFS
import os


def mock_run_pdf_latex(command):
    if command[0] != 'pdflatex':
        return

    output_directory = command[2]  # output directory
    tex_path = command[3]
    pdf_path = tex_path[:-3] + 'pdf'

    with open(pdf_path, 'w') as f:
        f.write('pdf content')


class FeaturePdfCreation:
    def given_a_pdf_creator(self):
        self.stub_template_fs = StubFS('/path')
        self.stub_template_fs.create_file('template.tex',
                                          r'This is a \VAR{a_variable}')
        return PdfContentCreator(self.stub_template_fs)

    @patch('conteng.pdf.run')
    def ucase_create_pdf_file(self, mock_run):
        pdf_creator = self.given_a_pdf_creator()
        mock_run.side_effect = mock_run_pdf_latex

        root_content = {'a_variable': 'value'}
        file_path = 'pdf_file.pdf'

        pdf_creator.create_content_file('template.tex', root_content,
                                        file_path)

        assert os.path.exists(file_path)
        os.remove(file_path)

    @patch('conteng.pdf.run')
    def ucase_create_pdf_content_calls_pdflatex(self, mock_run):
        pdf_creator = self.given_a_pdf_creator()
        mock_run.side_effect = mock_run_pdf_latex

        root_content = {'a_variable': 'value'}
        pdf_creator.create_content('template.tex', root_content)

        temp_fs_path = pdf_creator.temp_fs.getsyspath('/')
        tex_path = self.get_tex_path(pdf_creator.temp_fs)
        mock_run.assert_called_with(['pdflatex',  '-output-directory',
                                     temp_fs_path, tex_path])

    def get_tex_path(self, fs):
        for file in fs.glob('*'):
            if file.path[-3:] == 'tex':
                tex_fn = file.path[1:]
        return os.path.join(fs.getsyspath('/'), tex_fn)

    @patch('conteng.pdf.run')
    def ucase_create_pdf_content_saves_properly_tex_content(self, mock_run):
        pdf_creator = self.given_a_pdf_creator()
        mock_run.side_effect = mock_run_pdf_latex

        root_content = {'a_variable': 'value'}
        content = pdf_creator.create_content(
            'template.tex', root_content)

        tex_path = self.get_tex_path(pdf_creator.temp_fs)
        with open(tex_path, 'r') as f:
            assert f.read() == 'This is a value'

    @patch('conteng.pdf.run')
    def ucase_create_pdf_content_generate_correct_pdf(self, mock_run):
        pdf_creator = self.given_a_pdf_creator()
        mock_run.side_effect = mock_run_pdf_latex

        root_content = {'a_variable': 'value'}
        content = pdf_creator.create_content('template.tex', root_content)

        assert content == b'pdf content'
