from unittest.mock import Mock
import conteng.tex as _
import os


class FakeFile:
    def __init__(self, content):
        self.content = content

    def __enter__(self, ):
        file = Mock()
        file.read.return_value = self.content

        return file

    def __exit__(self, type, value, traceback):
        pass


class FeatureTemplateContents:
    def _given_a_content_generator(self):
        self.template_fs = Mock()
        self.template_fs.open.return_value = FakeFile(
            r'The number is \VAR{number} and the string is "\VAR{string}"')
        return _.TexContentCreator(self.template_fs)

    def ucase_render_content_from_simple_file(self):

        content_generator = self._given_a_content_generator()
        root_content = {'number': 1, 'string': 'a_string'}
        content = content_generator.render('file.txt', root_content)

        assert content == 'The number is 1 and the string is "a_string"'

    def ucase_templates_are_cached(self):
        content_generator = self._given_a_content_generator()

        root_content = {'number': 1, 'string': 'a_string'}
        content_generator.render('file.txt', root_content)
        content_generator.render('file.txt', root_content)

        assert len(self.template_fs.open.mock_calls) == 1

    def ucase_render_content_to_file(self):
        content_generator = self._given_a_content_generator()

        root_content = {'number': 1, 'string': 'a_string'}
        content_generator.render('file.txt', root_content, 'out_file.txt')

        assert os.path.exists('out_file.txt')
        os.remove('out_file.txt')
