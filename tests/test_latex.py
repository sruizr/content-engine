import os
import conteng.latex as _
from tests.builders import StubFS


def my_filter(value):
    return value + 'ff'


class FeatureTemplateContents:
    def _given_a_content_generator(self):
        self.template_fs = StubFS('/path')
        self.template_fs.create_file(
            'template.txt',
            r'The number is \VAR{number} and the string is "\VAR{string | key}"')

        creator = _.LatexContentCreator(self.template_fs)
        creator.add_filter("key", my_filter)
        return creator

    def ucase_render_content_from_simple_file(self):

        content_generator = self._given_a_content_generator()
        root_content = {'number': 1, 'string': 'a_string'}
        content = content_generator.create_content('template.txt', root_content)

        assert content == 'The number is 1 and the string is "a_stringff"'

    def ucase_templates_are_cached(self):
        content_generator = self._given_a_content_generator()

        root_content = {'number': 1, 'string': 'a_string'}
        content_generator.create_content('template.txt', root_content)
        content_generator.create_content('template.txt', root_content)

        assert self.template_fs.open_calls == 1

    def ucase_render_content_to_file(self):
        content_generator = self._given_a_content_generator()

        root_content = {'number': 1, 'string': 'a_string'}
        content_generator.create_content_file('template.txt', root_content,
                                              'out_file.txt')

        assert os.path.exists('out_file.txt')
        with open('out_file.txt', 'r') as f:
            assert f.read() == 'The number is 1 and the string is "a_stringff"'
        os.remove('out_file.txt')
