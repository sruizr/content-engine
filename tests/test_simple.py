import os
import conteng.simple as _
from tests.builders import StubFS


class FeatureTemplateContents:
    def given_a_simple_content_creator(self):
        self.template_fs = StubFS('/path')
        self.template_fs.create_file(
            'template.txt',
            'The number is {{number}} and the string is "{{string}}"')
        return _.SimpleContentCreator(self.template_fs)

    def ucase_render_content_from_simple_file(self):

        content_creator = self.given_a_simple_content_creator()

        root_content = {'number': 1, 'string': 'a_string'}
        content = content_creator.create_content('template.txt', root_content)

        assert content == 'The number is 1 and the string is "a_string"'

    def ucase_templates_are_cached(self):
        content_creator = self.given_a_simple_content_creator()

        root_content = {'number': 1, 'string': 'a_string'}
        value = content_creator.create_content('template.txt', root_content)
        value = content_creator.create_content('template.txt', root_content)

        assert self.template_fs.open_calls == 1

    def ucase_create_file_with_content(self):
        content_creator = self.given_a_simple_content_creator()

        root_content = {'number': 1, 'string': 'a_string'}
        content_creator.create_content_file('template.txt', root_content, 'out_file.txt')

        with open('out_file.txt', 'r') as f:
            assert f.read() == 'The number is 1 and the string is "a_string"'

        os.remove('out_file.txt')
