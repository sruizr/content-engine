from jinja2.loaders import BaseLoader, TemplateNotFound
from jinja2 import Environment

import conteng.formats as formats


class FSLoader(BaseLoader):
    """Loader for FileSystem object (PyFileSystem interface)
    """

    def __init__(self, fs):
        "docstring"
        self.fs = fs

    def get_source(self, environment, template_path):
        print(template_path)
        if not self.fs.exists(template_path):
            raise TemplateNotFound(template_path)

        with self.fs.open(template_path, 'r') as f:
            source = f.read()

        return source, template_path, lambda: True


class LatexContentCreator:
    """Create tex content from tex templates
    """
    env_config = {
        'block_start_string': r'\BLOCK{',
        'block_end_string': '}',
        'variable_start_string': r'\VAR{',
        'variable_end_string': '}',
        'comment_start_string': r'\#{',
        'comment_end_string': '}',
        'line_statement_prefix': '%%',
        'line_comment_prefix': '%#',
        'trim_blocks': True,
        'autoescape': False
    }

    def __init__(self, template_fs):
        self.engine = Environment(loader=FSLoader(template_fs),
                                  **self.env_config)
        self.engine.filters = {'format_datetime': formats.format_datetime,
                               'format_decimal': formats.format_decimal,
                               'format_timdelta': formats.format_timedelta}

    @property
    def filters(self):
        return self.engine.filters.copy()

    def add_filter(self, key, filter_fun):
        self.engine.filters[key] = filter_fun

    def create_content(self, template_name, root_content):
        """Fill data on template and return a temporary file path
        """
        template = self.engine.get_template(template_name)
        return template.render(**root_content)

    def create_content_file(self, template_name, root_content, out_file_path):
        content = self.create_content(template_name, root_content)

        with open(out_file_path, 'w') as f:
            f.write(content)
