class SimpleContentCreator:

    def __init__(self, templates_fs):
        """Create of contents from simple dictionaries using a templates location
        """
        self._cached_templates = {}
        self._fs = templates_fs

    def create_content(self, template_name, root_content):
        if template_name not in self._cached_templates:
            with self._fs.open(template_name, 'r') as f:
                self._cached_templates[template_name] = f.read()

        content = self._cached_templates[template_name]
        for key, value in root_content.items():
            content = content.replace('{{' + key + '}}', str(value))
        return content


    def create_content_file(self, template_name, root_content, out_filepath):
        content = self.create_content(template_name, root_content)
        with open(out_filepath, 'w') as f:
            f.write(content)
