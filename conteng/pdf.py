import logging
import subprocess
from uuid import uuid4

from conteng.latex import LatexContentCreator
from fs.tempfs import TempFS

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)


class PdfContentCreator:
    def __init__(self, template_fs):
        self.tex_creator = LatexContentCreator(template_fs)
        self.temp_fs = TempFS()

    @property
    def filters(self):
        return self.tex_creator.filters

    def add_filter(self, key, fun):
        self.tex_creator.add_filter(key, fun)

    def create_content(self, template_name, root_content):
        name = uuid4().hex
        tex_path = self.temp_fs.getsyspath(name + '.tex')
        self.tex_creator.create_content_file(
            template_name, root_content, tex_path)
        run(['pdflatex', '-output-directory', self.temp_fs.getsyspath('/'),
             tex_path])

        pdf_path = self.temp_fs.getsyspath(name + '.pdf')
        with open(pdf_path, 'rb') as f:
            return f.read()

    def create_content_file(self, template_name, root_content, out_file_path):
        with open(out_file_path, 'wb') as f:
            f.write(self.create_content(template_name, root_content))


def run(command):
    try:
        result = subprocess.run(
            command,
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE,
            check=True,
        )
        logger.debug(result.stdout)
    except subprocess.CalledProcessError as e:
        logger.error(e)
        logger.error(result.stderr)
        raise e
