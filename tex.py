from jinja2.loaders import BaseLoader, TemplateNotFound
from jinja2 import Environment


class FSLoader(BaseLoader):
    """Loader for FileSystem object (PyFileSystem interface)
    """

    def __init__(self, fs):
        "docstring"
        self.fs = fs

    def get_source(self, environment, template_path):
        if not self.fs.exists(template_path):
            raise TemplateNotFound(template_path)

        with self.fs.open(template_path, 'r') as f:
            source = f.read()

        return source, template_path, lambda: True


class TexContentCreator:
    """Create tex content from tex templates
    """
    env_config = {
        'block_start_string': r'\BLOCK{',
        'block_end_string': '}',
        'variable_start_string': r'\VAR{',
        'variable_end_string': '}',
        'comment_start_string': r'\#{',
        'comment_end_string': '}',
        'line_statement_prefix': '%%',
        'line_comment_prefix': '%#',
        'trim_blocks': True,
        'autoescape': False
    }

    def __init__(self, template_fs, filters=None):
        self.engine = Environment(loader=FSLoader(template_fs),
                                  **self.env_config)
        if filters:
            self.engine.filters = self._get_filters(filters)

    def render(self, template_name, root_content, file_path=None):
        """Fill data on template and return a temporary file path
        """
        template = self.engine.get_template(template_name)
        content = template.render(**root_content)

        if not file_path:
            return content

        with open(file_path, 'w') as f:
            f.write(content)
